<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
	<script type="text/javascript" src="${ctx}/webresource/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="${ctx}/webresource/js/core.js" ></script>
	<script type="text/javascript" src="${ctx}/webresource/js/tripledes.js" ></script>
	<script type="text/javascript" src="${ctx}/webresource/js/mode-ecb.js"></script>
</head>
<body>
	<!--  本页面的功能是进行前端js加密，提交到后端，后端采用相同的算法进行解密 -->
	<form id="form1" action="${ctx}/User/login/phone" method="post">
		<!--  该秘钥可以在后期进行动态的生成，要求长度必须是8的倍数  -->
		<input type="hidden" id="seckey" name="seckey" value="12345678"/>
		key:<input type="text" name="key" value="${userSignupParam.key }" />
		passwd:<input type="text" id="passwd" name="passwd" value="${userSignupParam.passwd }" />
		phone:<input type="text" id="phone" name="phone" value="${userSignupParam.phone }" />

		check:<input type="text" id="check" name="check" value="${userSignupParam.check }" />
		<div>
			<input type="button" value="save" name="save" onclick="submitForm();"/>
			<input type="button" value="cancle" name="cancle" onclick="cancleForm();"/>
		</div>
	</form>

<script type="text/javascript">
	function submitForm() {

		var passwd = document.getElementById("passwd").value;
		if (!passwd) {
			return false;
		}
		var phone = document.getElementById("phone").value;
		if (!phone) {
			return false;
		}
		var check = document.getElementById("check").value;
		if (!check) {
			return false;
		}

		var newPasswd = encryptByDES(passwd, $("#seckey").val());
		document.getElementById("passwd").value = newPasswd;

		document.getElementById("form1").submit();

	}

	// DES加密
	function encryptByDES(message, key) {
		var keyHex = CryptoJS.enc.Utf8.parse(key);
		var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
			mode: CryptoJS.mode.ECB,
			padding: CryptoJS.pad.Pkcs7
		});
		return encrypted.toString();
	}
</script>
</body>
</html>