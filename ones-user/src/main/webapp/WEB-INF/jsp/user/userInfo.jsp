<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
		<table>
			<tr>
				<td>
					uid:<input type="text" name="uid" value="${userInfo.uid }" />
					passwd:<input type="text" name="passwd" value="${userInfo.passwd }" />
					lname:<input type="text" name="lname" value="${userInfo.lname }" />
				</td>
			</tr>
			<tr>
				<td>
					nname:<input type="text" name="nname" value="${userInfo.nname }" />
					avatar:<input type="text" name="avatar" value="${userInfo.avatar }" />
					phone:<input type="text" name="phone" value="${userInfo.phone }" />
				</td>
			</tr>
			<tr>
				<td>
					email:<input type="text" name="email" value="${userInfo.email }" />
					utype:<input type="text" name="utype" value="${userInfo.utype }" />
					status:<input type="text" name="status" value="${userInfo.status }" />
				</td>
			</tr>
		</table>

</body>
</html>