<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="${ctx}/User/signup/base/user.web" method="post">
		uid:<input type="text" name="uid" value="${userSignupParam.uid }" />
		passwd:<input type="text" name="passwd" value="${userSignupParam.passwd }" />
		lname:<input type="text" name="lname" value="${userSignupParam.lname }" />

		nname:<input type="text" name="nname" value="${userSignupParam.nname }" />
		avatar:<input type="text" name="avatar" value="${userSignupParam.avatar }" />
		phone:<input type="text" name="phone" value="${userSignupParam.phone }" />

		email:<input type="text" name="email" value="${userSignupParam.email }" />
		utype:<input type="text" name="utype" value="${userSignupParam.utype }" />
		status:<input type="text" name="status" value="${userSignupParam.status }" />
		<input type="submit" name="save" value="save" />
	</form>
</body>
</html>