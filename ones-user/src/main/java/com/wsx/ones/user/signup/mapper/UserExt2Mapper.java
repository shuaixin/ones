package com.wsx.ones.user.signup.mapper;

import com.wsx.ones.user.model.UserExt2;

/**
 * Created by wangshuaixin on 16/12/27.
 */
public interface UserExt2Mapper {

    int saveUserExt2(UserExt2 userExt2);
}
