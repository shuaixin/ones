package com.wsx.ones.user.settings.service.impl;

import com.wsx.ones.user.settings.service.UserSettingsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/13.
 */
@Service
@Transactional
public class UserSettingsServiceImpl implements UserSettingsService {
}
