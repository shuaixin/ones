package com.wsx.ones.user.login;

import com.wsx.ones.core.secure.DESUtils;
import com.wsx.ones.user.UserBaseController;
import com.wsx.ones.user.UserLoginEnum;
import com.wsx.ones.user.UserParam;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 17/1/18.
 */
@RestController
@Scope("prototype")
public class PhoneLoginController extends UserBaseController {

    private final static Logger log = LoggerFactory.getLogger(PhoneLoginController.class);


    @RequestMapping(
            value = "/login/phone",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData phoneLogin(UserParam userParam) {
        log.info("enter");

        if (!checkLoginParam(userParam, UserLoginEnum.PHONE)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        //解密的key后期可以动态的生成，并且做到安全，可以定期的更换，放置在redis中
        String passwd = userParam.getPasswd();
        String val = DESUtils.decryptDes(passwd,"12345678");




        return new OutputData();
    }
}
