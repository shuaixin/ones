package com.wsx.ones.user;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public enum UserLoginEnum {

    //不作为
    NONE,
    //登录名
    LNAME,
    //手机
    PHONE,
    //邮箱
    EMAIL

}
