package com.wsx.ones.user.rpc.inter;

import com.wsx.ones.user.rpc.model.TokenUser;

public interface UserCommonInterface {

	//test
	public String get();

	//验证用户token
	public TokenUser checkUser(String token);
}
