package com.wsx.ones.custom.center.dao.impl;

import com.wsx.ones.custom.center.dao.CenterDao;
import com.wsx.ones.custom.center.mapper.CenterMapper;
import com.wsx.ones.custom.center.model.CustomNotice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 17/1/4.
 */
@Repository
public class CenterDaoImpl implements CenterDao {

    @Autowired
    private CenterMapper centerMapper;


    public int saveNotice(CustomNotice notice) {
        return centerMapper.saveNotice(notice);
    }

    @Override
    public List<Map<String, Object>> getFiveNotices() {
        return centerMapper.getFiveNotices();
    }
}
