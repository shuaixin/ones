package com.wsx.ones.custom.center.vo;

import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.BaseVo;

/**
 * Created by wangshuaixin on 17/1/4.
 */
public class CenterVo extends BaseVo {

    private Long id;

    public CenterVo() {
        super();
    }
    public CenterVo(ReturnStatus returnStatus) {
        super(returnStatus);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
