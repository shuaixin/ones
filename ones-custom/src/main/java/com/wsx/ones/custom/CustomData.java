package com.wsx.ones.custom;

import com.wsx.ones.web.model.OutputData;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public class CustomData extends OutputData {

    private static final long serialVersionUID = 4147905131039195087L;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
