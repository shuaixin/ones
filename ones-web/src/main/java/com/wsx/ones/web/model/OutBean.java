package com.wsx.ones.web.model;

import com.wsx.ones.web.BoVo;
import com.wsx.ones.web.swap.DatabaseStatus;

/**
 * 主要处理层数据返回的bean，需要返回bean时才使用
 * Created by wangshuaixin on 16/12/23.
 */
public interface OutBean extends BoVo {

    public DatabaseStatus getDatabaseStatus();

    public Exception getDatabaseError();
}
