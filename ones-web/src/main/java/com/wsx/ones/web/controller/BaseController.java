package com.wsx.ones.web.controller;

import com.wsx.ones.web.model.BaseUser;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.model.PageParam;
import com.wsx.ones.web.vo.BaseVo;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by wangshuaixin on 16/12/15.
 */
@RestController
public abstract class BaseController {

    @ExceptionHandler
    public String exceptionHandler(Exception ex, Model model) {
        model.addAttribute("message", ex.getMessage());
        return "error";
    }

    /**
     * 验证用户token是否有效
     * @param token
     * @return
     */
    protected boolean checkToken(String token) {
        if (null == token) {
            return false;
        }
        if (token.length() <= 10) {
            return false;
        }
        return true;
    }

    /**
     * 分页查询的验证
     * @param pageParam
     * @return
     */
    protected boolean checkPageParam(PageParam pageParam) {
        if (!checkToken(pageParam.getToken())) {
            return false;
        }
        if (null == pageParam.getPageNo() || pageParam.getPageNo() <= 0) {
            pageParam.setPageNo(1);
        }
        if (null == pageParam.getPageSize() || pageParam.getPageSize() <= 0) {
            pageParam.setPageSize(10);
        }

        return true;
    }

    public abstract BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache);

    public abstract BaseUser getUserCache(String uid, boolean ifEhcache);

    /**
     * 返回值的抽象化，保证功能的完成及代码的复用，每个模块可以单独做各个的业务返回公用话
     * @param baseVo service返回的vo
     * @param clazz 需要转化的bean
     * @return 返回页面需要的数据
     */
    public abstract OutputData checkVo(BaseVo baseVo, Class<? extends OutputData> clazz);

    /**
     * 对于操作成功后的业务逻辑处理，子类可以重写该方法完成业务逻辑的实现
     * 要求每个api的设计只有一个方法，需要将该方法单独出来一个类
     * @param baseVo
     */
    public void doSuccessVo(BaseVo baseVo) {}

    public void doSuccessVo(BaseVo baseVo, HttpServletResponse response) {}

    /**
     * 对于操作失败的业务逻辑处理，子类可以重写该方法完成对应业务逻辑的处理
     * 要求每个api的设计只有一个方法，需要将该方法单独出来一个类
     * @param baseVo
     */
    public void doErrorVo(BaseVo baseVo) {}
}
