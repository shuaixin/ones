package com.wsx.ones.web.container;

import org.springframework.util.StringUtils;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public final class TokenContainer {

    private static ThreadLocal<String> tokenLocal = new ThreadLocal<String>();

    private TokenContainer() {
    }

    public static String getToken() {
        return tokenLocal.get();
    }

    public static void setToken(String token) {
        if (StringUtils.isEmpty(token)) {
            tokenLocal.set(token);
        }
    }

    public static void delToken() {
        tokenLocal.remove();
    }
}
