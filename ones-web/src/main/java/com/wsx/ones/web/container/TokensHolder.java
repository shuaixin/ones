package com.wsx.ones.web.container;

import com.wsx.ones.finalstr.common.CommonFinalUtil;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public final class TokensHolder {

    private static Map<Thread, String> tokens = new ConcurrentHashMap<Thread, String>();

    private TokensHolder() {}

    public static String get() {
        if (tokens.containsKey(Thread.currentThread())) {
            return tokens.get(Thread.currentThread());
        }
        return CommonFinalUtil.STRING_EMPTY;
    }

    public static void set(String token) {
        if (!tokens.containsKey(Thread.currentThread())) {
            tokens.put(Thread.currentThread(), token);
        }
    }

    public static void del() {
        if (tokens.containsKey(Thread.currentThread())) {
            tokens.remove(Thread.currentThread());
        }
    }
}
