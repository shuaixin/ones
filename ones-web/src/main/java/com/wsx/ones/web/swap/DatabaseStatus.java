package com.wsx.ones.web.swap;

/**
 * Created by wangshuaixin on 16/12/23.
 */
public enum DatabaseStatus {

    /** 执行成功 */
    SUCCESS,

    /** 执行成功但无结果 */
    NO_RESULT,

    /** 执行失败 */
    ERROR;
}
