package com.wsx.ones.web.util;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public enum OperationEnum {

    //新增
    ADD,
    //更新
    UPDATE,
    //删除
    DELETE,
    //查找
    GET
}
