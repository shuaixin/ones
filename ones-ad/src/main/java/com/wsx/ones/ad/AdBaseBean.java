package com.wsx.ones.ad;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/9.
 */
public class AdBaseBean extends BaseBean {

    private static final long serialVersionUID = 1567392992444478115L;

    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
