package com.wsx.ones.ad.model;

import com.wsx.ones.ad.AdBaseBean;

/**
 * Created by wangshuaixin on 17/1/3.
 */
public class AdPosition extends AdBaseBean {

    private static final long serialVersionUID = -3104029619596151689L;

    private Long pid;
    private String pname;
    private Double width;
    private Double height;
    private String memo;
    private String style;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
