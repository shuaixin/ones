package com.wsx.ones.ad.index.model;

import com.wsx.ones.ad.AdBaseBean;

/**
 * Created by wangshuaixin on 16/12/9.
 */
public class Ad extends AdBaseBean {

    private static final long serialVersionUID = 5621907250140130374L;

    private String test;

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
