package com.wsx.ones.hello;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/10.
 */
public class HelloBaseBean extends BaseBean {

    private static final long serialVersionUID = 8796955923905170636L;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
