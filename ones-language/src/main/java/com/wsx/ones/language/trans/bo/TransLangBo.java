package com.wsx.ones.language.trans.bo;

import com.wsx.ones.web.bo.BaseBo;

/**
 * Created by wangshuaixin on 17/1/5.
 */
public class TransLangBo implements BaseBo {

    private String content;
    private String fromLang;
    private String toLang;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFromLang() {
        return fromLang;
    }

    public void setFromLang(String fromLang) {
        this.fromLang = fromLang;
    }

    public String getToLang() {
        return toLang;
    }

    public void setToLang(String toLang) {
        this.toLang = toLang;
    }
}
