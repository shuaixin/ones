package com.wsx.ones.language;

import com.wsx.ones.web.model.InputParam;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public class LanguageParam extends InputParam {

    private String content;
    private String fromLang;
    private String toLang;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFromLang() {
        return fromLang;
    }

    public void setFromLang(String fromLang) {
        this.fromLang = fromLang;
    }

    public String getToLang() {
        return toLang;
    }

    public void setToLang(String toLang) {
        this.toLang = toLang;
    }
}
