<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<h2>Hello World!</h2>

<hr/>

<form action="${ctx}/Language/trans/en" method="post">
    content:<input type="text" name="content" value="你好" />
    fromLang:<input type="text" name="fromLang" value="zh" />
    toLang:<input type="text" name="toLang" value="en" />
    <input type="submit" name="save" value="save" />
</form>
</body>
</html>

