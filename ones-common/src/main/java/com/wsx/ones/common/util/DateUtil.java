package com.wsx.ones.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具
 */
public class DateUtil {

	public static final String YYYYMMDDHHMMSS = "yyyy-MM-dd hh:mm:ss";
	public static final String YYYYMMDD = "yyyy-MM-dd";


	/**
	 * 获得当前时间，类型是Date
	 * @return
	 */
	public static Date getDate() {
		return new Date();
	}

	/**
	 * 获得当前时间，类型是字符串的格式
	 * @return
	 */
	public static String getNowDate() {
		return getNowDate(YYYYMMDDHHMMSS);
	}

	/**
	 * 获得当前时间，类型是指定字符串的格式
	 * @param format 模式
	 * @return
	 */
	public static String getNowDate(String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(getDate());
	}


	/**
	 * 时间类型转换成字符串类型的结果返回
	 * @param date
	 * @return
	 */
	public static String getStringDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYYMMDDHHMMSS);
		return dateFormat.format(date);
	}

	/**
	 * 字符串类型转换成日期类型
	 * @param dateStr
	 * @return
	 */
	public static Date getDateFromStr(String dateStr) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(YYYYMMDD);
		Date date = null;
		try {
			date = dateFormat.parse(dateStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 获得当前时间的时间戳
	 * @return
	 */
	public static Long getNowStamp() {
		return getDate().getTime();
	}
}
