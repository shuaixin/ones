package com.wsx.ones.settings.system;

import com.wsx.ones.settings.SettingsBaseController;
import com.wsx.ones.settings.SettingsData;
import com.wsx.ones.settings.system.bo.SystemBo;
import com.wsx.ones.settings.system.param.SystemParam;
import com.wsx.ones.settings.system.service.SystemService;
import com.wsx.ones.settings.system.vo.SystemVo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.PojoCopy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/11.
 */
@RestController
@Scope("prototype")
public class SystemController extends SettingsBaseController {

    @Autowired
    private SystemService systemService;

    @RequestMapping(
            value = "/test/set",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData settingsTest(SystemParam systemParam) {

        if (!checkSetParams(systemParam)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        SystemBo bo = PojoCopy.copy(systemParam, SystemBo.class);
        bo.setId(123);
        SystemVo vo = systemService.saveTest(bo);
        /**
        switch (vo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            case DATA_EXIST:
            default:
                break;
        }

        return PojoCopy.copy(vo, SettingsData.class);
         */
        return checkVo(vo, SettingsData.class);
    }
}
