package com.wsx.ones.settings;

import com.wsx.ones.web.model.OutputData;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public class SettingsData extends OutputData {

    public SettingsData () {
        super();
    }
    public SettingsData (int code, int status) {
        super(code, status);
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
