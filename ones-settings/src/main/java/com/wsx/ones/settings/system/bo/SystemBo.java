package com.wsx.ones.settings.system.bo;

import com.wsx.ones.web.bo.BaseBo;

/**
 * Created by wangshuaixin on 17/1/8.
 */
public class SystemBo implements BaseBo {

    private static final long serialVersionUID = -6915896095893258223L;

    private Integer id;
    private String test;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
