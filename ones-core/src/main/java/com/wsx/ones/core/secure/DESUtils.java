package com.wsx.ones.core.secure;

import org.bouncycastle.util.encoders.Base64;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * 处理前端通过js进行des的加密处理，后端进行数据解密的操作
 * Created by wangshuaixin on 17/1/19.
 */
public class DESUtils {

    private static final String ALGORITHM_DES = "DES"; //定义 加密算法,可用 DES,DESede,Blowfish
    //加密的key的长度必须是8的倍数，否则报错
    private static final String PUBLIC_KEY = "_o_1op0ones_wvm8wsxvu_o6";

    public static final String ENCODING = "UTF-8";

    /**
     * 加密操作，接收数组
     * @param source 原文
     * @param seckey 秘钥
     * @return
     */
    public static byte[] encryptDes(byte[] source, byte[] seckey) {

        Cipher cipher = null;
        try {
            SecretKey secretKey = new SecretKeySpec(seckey, ALGORITHM_DES);
            cipher = Cipher.getInstance(ALGORITHM_DES);

            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            return cipher.doFinal(source);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 解密处理
     * @param source 密文
     * @param seckey 秘钥
     * @return
     */
    public static byte[] decryptDes(byte[] source, byte[] seckey) {

        Cipher cipher = null;
        try {
            SecretKey secretKey = new SecretKeySpec(seckey, ALGORITHM_DES);
            cipher = Cipher.getInstance(ALGORITHM_DES);

            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            return cipher.doFinal(source);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 加密处理，接收的和返回的都是字符串，便于系统中的操作和转换
     * @param source 原文
     * @param key 秘钥
     * @return
     */
    public static String encryptDes(String source, String key) {
        try {
            byte[] src = source.getBytes(ENCODING);
            byte[] keys = key.getBytes(ENCODING);
            return new String(Base64.encode(decryptDes(src, keys)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密操作，接收的返回的都是字符串
     * @param source 密文
     * @param key 秘钥
     * @return
     */
    public static String decryptDes(String source, String key) {
        try {
            byte[] src = source.getBytes(ENCODING);
            byte[] keys = key.getBytes(ENCODING);
            return new String(decryptDes(Base64.decode(src),keys), ENCODING);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Deprecated
    public static void main(String[] args) throws Exception {
        //秘钥必须是8的倍数
        String key = "12345678";
        String source = "wsx123456";

        byte[] sec = encryptDes(source.getBytes(ENCODING), key.getBytes(ENCODING));


        String newVal = new String(Base64.encode(sec),ENCODING);

        byte[] origin = decryptDes(Base64.decode(newVal.getBytes(ENCODING)), key.getBytes(ENCODING));

        String oriVal = new String(origin, ENCODING);

        System.out.print(oriVal);

        //P3OXNkyYf7sRUsQcU35IoQ==
        //zlN2U/tBCeK8gS3zNjQJ0w==

        //fCVIf1V/Bn4xhcIxeOGSZQ==
        //fCVIf1V/Bn4xhcIxeOGSZQ==
    }
}
