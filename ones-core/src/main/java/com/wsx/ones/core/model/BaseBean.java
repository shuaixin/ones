package com.wsx.ones.core.model;


public abstract class BaseBean implements Bean {

	private static final long serialVersionUID = 6587263316959904903L;
	
	private String _id;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}
}
