package com.wsx.ones.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 任务调度的抽象任务设计
 * Created by wangshuaixin on 17/1/20.
 */
public abstract class BaseJob implements Job {


    /**
     *
     * @param jobExecutionContext
     */
    private boolean beforeExecute(JobExecutionContext jobExecutionContext) {
        return true;
    }

    /**
     * 执行任务的操作，该类为了完整新，禁止被重写
     * @param jobExecutionContext
     * @throws JobExecutionException
     */
    public final void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        //
        if (!beforeExecute(jobExecutionContext)) {
            return;
        }

        try {
            realExecute(jobExecutionContext);
        } catch (Exception e) {

        } finally {
            //保证该代码的绝对运行
            afterExecute(jobExecutionContext);
        }

    }

    /**
     *
     * @param jobExecutionContext
     */
    protected abstract void realExecute(JobExecutionContext jobExecutionContext);


    /**
     *
     * @param jobExecutionContext
     */
    private void afterExecute(JobExecutionContext jobExecutionContext) {

    }

}
