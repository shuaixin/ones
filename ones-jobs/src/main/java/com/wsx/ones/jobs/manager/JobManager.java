package com.wsx.ones.jobs.manager;

import com.wsx.ones.jobs.model.JobTask;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 任务的设计思想：
 * 1，该任务是需要配置在spring中的，然后其他的任务可以参照spring的配置进行数据库级别的维护
 * 2，在这里查询数据库中的任务，分别创建任务调度器去执行任务
 * Created by wangshuaixin on 17/1/20.
 */
public class JobManager {

    /**
     * 任务的执行方
     */
    public void execute() {

        //TODO 获得要执行的任务
        List<JobTask> tasks = new ArrayList<JobTask>();

        //
        SchedulerFactory factory = new StdSchedulerFactory();

        //
        Scheduler scheduler = null;
        try {
            scheduler = factory.getScheduler();


            int taskSize = tasks.size();

            for (int i = 0; i < taskSize; i++) {
                JobTask jobTask = tasks.get(i);
                //启动任务
                startNewJob(scheduler, jobTask);
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 执行的真实任务
     * @param scheduler
     */
    private void startNewJob(Scheduler scheduler, JobTask jobTask) throws Exception {

        Class jobClass = Class.forName(jobTask.getClazz());

        JobDetail jobDetail = new JobDetail(jobTask.getCode(), Scheduler.DEFAULT_GROUP, jobClass);

        CronTrigger cronTrigger = new CronTrigger(jobTask.getCode(), Scheduler.DEFAULT_GROUP);
        cronTrigger.setCronExpression(new CronExpression(jobTask.getCron()));

        scheduler.scheduleJob(jobDetail, cronTrigger);
    }
}
