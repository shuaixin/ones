package com.wsx.ones.jobs.job;

import com.wsx.ones.jobs.BaseJob;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wangshuaixin on 17/1/20.
 */
public class TestJob extends BaseJob {

    private static final Logger log = LoggerFactory.getLogger(TestJob.class);

    @Override
    protected void realExecute(JobExecutionContext jobExecutionContext) {
        log.info("start job");
    }
}
