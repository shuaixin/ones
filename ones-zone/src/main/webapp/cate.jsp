<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="${ctx}/Zone/cate/add" method="post">
		name:<input type="text" name="name" value="${categoryParam.name }" />
		sindex:<input type="text" name="sindex" value="${categoryParam.sindex }" />
		uid:<input type="text" name="uid" value="${categoryParam.uid }" />
		<input type="submit" name="save" value="save" />
	</form>

	<br/>
	<hr/>

	<form action="${ctx}/Zone/cate/catch" method="post">
		token:<input type="text" name="token" value="${pageParam.token }" />
		pageNo:<input type="text" name="pageNo" value="${pageParam.pageNo }" />
		pageSize:<input type="text" name="pageSize" value="${pageParam.pageSize }" />
		<input type="submit" name="save" value="save" />
	</form>

	<br/>
	<hr/>

	<form action="${ctx}/Zone/cate/search" method="post">
		token:<input type="text" name="token" value="${cateSearchParam.token }" />
		pageNo:<input type="text" name="pageNo" value="${cateSearchParam.pageNo }" />
		pageSize:<input type="text" name="pageSize" value="${cateSearchParam.pageSize }" />
		name:<input type="text" name="name" value="${cateSearchParam.name }" />
		atime:<input type="text" name="atime" value="${cateSearchParam.atime }" />
		<input type="submit" name="save" value="save" />
	</form>
</body>
</html>