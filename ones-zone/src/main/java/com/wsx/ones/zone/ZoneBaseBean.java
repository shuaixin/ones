package com.wsx.ones.zone;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/11.
 */
public class ZoneBaseBean extends BaseBean {

    private static final long serialVersionUID = 7624825525390757158L;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
