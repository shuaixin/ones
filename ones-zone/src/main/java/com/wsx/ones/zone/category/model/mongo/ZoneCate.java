package com.wsx.ones.zone.category.model.mongo;

import com.wsx.ones.core.model.BaseBean;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/29.
 */
@Document(collection = "zone_cate")
public class ZoneCate extends BaseBean {

    private static final long serialVersionUID = -6575893141117336673L;

    private String name;
    private Integer sindex;
    private Date atime;
    private Long uid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSindex() {
        return sindex;
    }

    public void setSindex(Integer sindex) {
        this.sindex = sindex;
    }

    public Date getAtime() {
        return atime;
    }

    public void setAtime(Date atime) {
        this.atime = atime;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
