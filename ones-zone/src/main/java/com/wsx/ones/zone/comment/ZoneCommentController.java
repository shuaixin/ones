package com.wsx.ones.zone.comment;

import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.zone.comment.model.mongo.ZoneComment;
import com.wsx.ones.zone.comment.service.ZoneCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.zone.ZoneBaseController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@Scope("prototype")
public class ZoneCommentController extends ZoneBaseController {

    @Autowired
    private ZoneCommentService zoneCommentService;

    @RequestMapping(
            value = "/zone/save",
            method = {RequestMethod.POST}
    )
    public OutputData saveComment(ZoneComment zoneComment, HttpServletRequest request, HttpServletResponse response) {

        boolean isSave = zoneCommentService.saveComment(zoneComment);
        if (!isSave) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        return new OutputData();
    }


}
