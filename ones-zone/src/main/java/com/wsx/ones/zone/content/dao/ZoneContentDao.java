package com.wsx.ones.zone.content.dao;

import com.wsx.ones.zone.content.model.mongo.ZoneContent;

public interface ZoneContentDao {

	public boolean saveZoneContent(ZoneContent zoneContent);
}
