package com.wsx.ones.zone.content.vo;

import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.zone.ZoneVo;

/**
 * Created by wangshuaixin on 16/12/27.
 */
public class ContentVo extends ZoneVo {

    private static final long serialVersionUID = 6225395003587772666L;

    public ContentVo() {
        super();
    }

    public ContentVo(ReturnStatus returnStatus) {
        super(returnStatus);
    }
}
