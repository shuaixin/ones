package com.wsx.ones.pay.account.dao;

import com.wsx.ones.pay.account.model.PayTest;

/**
 * Created by wangshuaixin on 16/12/14.
 */
public interface AccountDao {

    int savePay(PayTest payTest);

    int saveTest(PayTest test);
}
