package com.wsx.ones.pay.account;

import com.wsx.ones.pay.PayBaseController;
import com.wsx.ones.pay.PayData;
import com.wsx.ones.pay.PayParam;
import com.wsx.ones.pay.account.bo.AccountBo;
import com.wsx.ones.pay.account.service.AccountService;
import com.wsx.ones.pay.account.vo.AccountVo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.PojoCopy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/14.
 */
@RestController
@Scope("prototype")
public class AccountController extends PayBaseController {

    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @RequestMapping(
            value = "/account/pay",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData paySave(PayParam payParam) {
        log.info("enter");

        if (!checkPayParam(payParam)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        AccountBo bo = PojoCopy.copy(payParam, AccountBo.class);

        AccountVo vo = accountService.savePay(bo);
        switch (vo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_SQL:
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            case DATA_EXIST:
            default:
                break;
        }

        return PojoCopy.copy(vo, PayData.class);
    }

    @RequestMapping(
            value = "/account/test",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData payTest(PayParam payParam) {
        log.info("enter 1");

        if (!checkPayParam(payParam)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        AccountBo bo = PojoCopy.copy(payParam, AccountBo.class);

        AccountVo vo = accountService.saveTest(bo);

        /**
        switch (vo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_SQL:
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            case DATA_EXIST:
                break;
            default:
                break;
        }

        return PojoCopy.copy(vo, PayData.class);
         */
        return checkVo(vo, PayData.class);
    }


}
