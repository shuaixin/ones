package com.wsx.ones.pay.account.model;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/14.
 */
public class PayTest extends BaseBean {

    private Integer pid;
    private Integer tid;
    private String tname;
    private String pname;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }
}
