package com.wsx.ones.search.es;

import com.wsx.ones.web.model.OutputData;

import java.util.List;
import java.util.Map;


/**
 * 
 * @author wangshuaixin
 *
 */
public class EsOutputDatas extends OutputData {

	private static final long serialVersionUID = -8990759960052491611L;
	
	private List<Map<String, Object>> datas;
	
	public EsOutputDatas() {
		super();
	}
	
	public EsOutputDatas(int code, int status) {
		super(code, status);
	}

	public List<Map<String, Object>> getDatas() {
		return datas;
	}

	public void setDatas(List<Map<String, Object>> datas) {
		this.datas = datas;
	}

}
