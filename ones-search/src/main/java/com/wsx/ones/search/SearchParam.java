package com.wsx.ones.search;

import com.wsx.ones.web.model.InputParam;

/**
 * Created by wangshuaixin on 16/12/24.
 */
public class SearchParam extends InputParam {

    private static final long serialVersionUID = 5494461647350403240L;

    private String name;

    private int from;
    private int size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
