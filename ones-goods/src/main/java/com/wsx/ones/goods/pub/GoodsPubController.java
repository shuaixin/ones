package com.wsx.ones.goods.pub;

import com.wsx.ones.goods.pub.model.GoodsPubBean;
import com.wsx.ones.goods.pub.service.GoodsPubService;
import com.wsx.ones.user.rpc.inter.UserCommonInterface;
import com.wsx.ones.user.rpc.model.TokenUser;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.goods.GoodsBaseController;


@RestController
@Scope("prototype")
public class GoodsPubController extends GoodsBaseController {

    @Autowired
    private GoodsPubService goodsPubService;

    @Autowired
    private UserCommonInterface userCommonInterface;

    @RequestMapping(
            value = "/pub/goods",
            method = {RequestMethod.POST}
    )
    public OutputData pubGoods(GoodsPubBean goodsPubBean) {
        if (!checkGoods(goodsPubBean)) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        String user = userCommonInterface.get();
        System.out.print("goods==" + user);

        TokenUser tokenUser = userCommonInterface.checkUser("goods");
        System.out.print(tokenUser.getName() + "--" + tokenUser.getNum());

        return new OutputData();
    }

}
