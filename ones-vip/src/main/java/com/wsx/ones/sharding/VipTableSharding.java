package com.wsx.ones.sharding;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.table.SingleKeyTableShardingAlgorithm;
import com.wsx.ones.finalstr.common.CommonFinalUtil;
import com.wsx.ones.finalstr.common.ShardingUtil;

import java.util.Collection;

/**
 * 分表算法，采用动态扩展的算法，每个表中的数据是固定的
 * 0 ~ 3000000 为vip_user_0, 3000000 ~ 6000000 为vip_user_2，一次类推
 * Created by wangshuaixin on 17/1/9.
 */
public class VipTableSharding implements SingleKeyTableShardingAlgorithm<Integer> {

    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        int table_num = shardingValue.getValue() / ShardingUtil.COUNT_PER_DB;
        for (String database : availableTargetNames) {
            int num = Integer.valueOf(database.substring(database.lastIndexOf(CommonFinalUtil.DOWN_SPIT) + 1));
            if (table_num == num) {
                return database;
            }

        }
        throw new IllegalArgumentException();
    }

    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }

    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }
}
