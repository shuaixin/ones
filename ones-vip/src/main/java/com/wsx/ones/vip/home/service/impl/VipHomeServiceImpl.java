package com.wsx.ones.vip.home.service.impl;

import com.wsx.ones.vip.home.bo.VipHomeBo;
import com.wsx.ones.vip.home.dao.VipHomeDao;
import com.wsx.ones.vip.home.model.VipHome;
import com.wsx.ones.vip.home.service.VipHomeService;
import com.wsx.ones.vip.home.vo.VipHomeVo;
import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.util.PojoCopy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/31.
 */
@Service
@Transactional
public class VipHomeServiceImpl implements VipHomeService{

    @Autowired
    private VipHomeDao vipHomeDao;

    @Transactional(propagation = Propagation.REQUIRED)
    public VipHomeVo saveVip(VipHomeBo bo) {

        VipHome home = PojoCopy.copy(bo, VipHome.class);
        int count = vipHomeDao.saveVip(home);
        if (count <= 0) {
            return new VipHomeVo(ReturnStatus.ERROR_UNKNOW);
        }

        VipHomeVo vo = new VipHomeVo();
        vo.setId(bo.getId());
        return vo;
    }
}
