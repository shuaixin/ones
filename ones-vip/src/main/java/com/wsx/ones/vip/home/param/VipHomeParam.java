package com.wsx.ones.vip.home.param;

import com.wsx.ones.vip.VipParam;

/**
 * Created by wangshuaixin on 17/1/9.
 */
public class VipHomeParam extends VipParam {

    private static final long serialVersionUID = -2715712098413806525L;

    private Integer vtype;
    private String name;

    public Integer getVtype() {
        return vtype;
    }

    public void setVtype(Integer vtype) {
        this.vtype = vtype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
