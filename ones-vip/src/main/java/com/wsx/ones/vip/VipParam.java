package com.wsx.ones.vip;

import com.wsx.ones.web.model.InputParam;

/**
 * Created by wangshuaixin on 16/12/31.
 */
public class VipParam extends InputParam {

    private static final long serialVersionUID = -5265811273642684578L;

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
