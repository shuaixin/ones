package com.wsx.ones.vip.home.model;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/17.
 */
public class VipHome extends BaseBean {

    private static final long serialVersionUID = -2409873735673443986L;

    private Integer id;
    private Integer vtype;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVtype() {
        return vtype;
    }

    public void setVtype(Integer vtype) {
        this.vtype = vtype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
