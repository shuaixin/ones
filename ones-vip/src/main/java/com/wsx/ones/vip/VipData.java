package com.wsx.ones.vip;

import com.wsx.ones.web.model.OutputData;

/**
 * Created by wangshuaixin on 16/12/31.
 */
public class VipData extends OutputData {

    private static final long serialVersionUID = -5703259986137529936L;

    public VipData () {
        super();
    }
    public VipData (int code, int status) {
        super(code, status);
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
