package com.wsx.ones.vip.home.service;

import com.wsx.ones.vip.home.bo.VipHomeBo;
import com.wsx.ones.vip.home.vo.VipHomeVo;

/**
 * Created by wangshuaixin on 16/12/17.
 */
public interface VipHomeService {

    VipHomeVo saveVip(VipHomeBo bo);
}
