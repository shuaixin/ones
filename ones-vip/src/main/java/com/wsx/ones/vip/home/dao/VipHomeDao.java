package com.wsx.ones.vip.home.dao;

import com.wsx.ones.vip.home.model.VipHome;

/**
 * Created by wangshuaixin on 16/12/17.
 */
public interface VipHomeDao {

    int saveVip(VipHome home);
}
